﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ConsumidorApi.Models
{
    public class OrdenServicio
    {
        public int id { get; set; }
        public int estado { get; set; }
        public int idUsuario { get; set; }
        public int idServicio { get; set; }
        public int idVehiculo { get; set; }
        public string placa { get; set; }
        public System.DateTime fecha { get; set; }
        public string comentario { get; set; }
    }
}