﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ConsumidorApi.Models
{
    public class Vehiculos
    {
        public int id { get; set; }
        public int estado { get; set; }
        public string marca { get; set; }
        public string modelo { get; set; }
        public int year { get; set; }
    }
}