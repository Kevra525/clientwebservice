﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ConsumidorApi.Models
{
    public class Usuarios
    {
        public int id { get; set; }
        public int estado { get; set; }
        public int tipo { get; set; }
        public string correo { get; set; }
        public string password { get; set; }
        public string nombre { get; set; }
        public string apellido { get; set; }
        public int dui { get; set; }
        public int telefono { get; set; }
    }
}