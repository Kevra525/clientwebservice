﻿using ConsumidorApi.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace ConsumidorApi.Controllers
{
    public class OrdenServicioController : Controller
    {
        string url = "http://localhost:58727/";

        // GET: listado de usuarios
        public async Task<ActionResult> ListaOrdenes()
        {
            List<OrdenServicio> empInfo = new List<OrdenServicio>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(url);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //llamar a todo los usuarios
                HttpResponseMessage rsp = await client.GetAsync("api/orden_servicio/");
                if (rsp.IsSuccessStatusCode)
                {
                    var empResponse = rsp.Content.ReadAsStringAsync().Result;
                    empInfo = JsonConvert.DeserializeObject<List<OrdenServicio>>(empResponse);
                }
            }

            return View(empInfo);
        }


        //POST: crear usuario
        public ActionResult createOrdenServicio()
        {
            return View();
        }

        [HttpPost]
        public ActionResult createOrdenServicio(OrdenServicio ordenesservicios)
        {

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:58727/api/usuarios/");
                var postTarea = client.PostAsJsonAsync<OrdenServicio>("ordenesservicios", ordenesservicios);
                postTarea.Wait();
                var resultado = postTarea.Result;
                if (resultado.IsSuccessStatusCode)
                {
                    return RedirectToAction("ListaOrdenes");
                }
            }

            ModelState.AddModelError(string.Empty, "ALgo salio mal");
            return View(ordenesservicios);
        }


        //PUT: editar usuarios
        public ActionResult EditOrdenServicio(int id)
        {
            OrdenServicio ordenesservicios = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:58727/");

                var responseTask = client.GetAsync("api/orden_servicio/" + id.ToString());
                responseTask.Wait();

                var resultado = responseTask.Result;
                if (resultado.IsSuccessStatusCode)
                {
                    var readTask = resultado.Content.ReadAsAsync<OrdenServicio>();
                    readTask.Wait();
                    ordenesservicios = readTask.Result;
                }
            }
            return View(ordenesservicios);
        }


        [HttpPost]
        public ActionResult EditOrdenServicio(OrdenServicio ordenesservicios)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:58727/");

                var putTask = client.PutAsJsonAsync($"api/orden_servicio/{ordenesservicios.id}", ordenesservicios);
                putTask.Wait();
                var resultado = putTask.Result;
                if (resultado.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }

            }
            return View(ordenesservicios);
        }

        //PUT: editar usuarios
        public ActionResult DeleteOrdenServicio(int id)
        {
            OrdenServicio ordenesservicios = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:58727/");

                var responseTask = client.GetAsync("api/orden_servicio/" + id.ToString());
                responseTask.Wait();

                var resultado = responseTask.Result;
                if (resultado.IsSuccessStatusCode)
                {
                    var readTask = resultado.Content.ReadAsAsync<OrdenServicio>();
                    readTask.Wait();
                    ordenesservicios = readTask.Result;
                }
            }
            return View(ordenesservicios);
        }


        [HttpPost]
        public ActionResult DeleteOrdenServicio(OrdenServicio ordenesservicios)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:58727/");

                var putTask = client.PutAsJsonAsync($"api/orden_servicio/{ordenesservicios.id}", ordenesservicios);
                putTask.Wait();
                var resultado = putTask.Result;
                if (resultado.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }

            }
            return View(ordenesservicios);
        }
    }
}