﻿using ConsumidorApi.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace ConsumidorApi.Controllers
{
    public class ServiciosController : Controller
    {
        string url = "http://localhost:58727/";

        // GET: listado de usuarios
        public async Task<ActionResult> Lista()
        {
            List<Servicios> empInfo = new List<Servicios>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(url);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //llamar a todo los usuarios
                HttpResponseMessage rsp = await client.GetAsync("api/servicios/");
                if (rsp.IsSuccessStatusCode)
                {
                    var empResponse = rsp.Content.ReadAsStringAsync().Result;
                    empInfo = JsonConvert.DeserializeObject<List<Servicios>>(empResponse);
                }
            }

            return View(empInfo);
        }


        //POST: crear usuario
        public ActionResult createServicio()
        {
            return View();
        }

        [HttpPost]
        public ActionResult createServicio(Servicios servicios)
        {

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:58727/api/servicios/");
                var postTarea = client.PostAsJsonAsync<Servicios>("servicios", servicios);
                postTarea.Wait();
                var resultado = postTarea.Result;
                if (resultado.IsSuccessStatusCode)
                {
                    return RedirectToAction("Lista");
                }
            }

            ModelState.AddModelError(string.Empty, "ALgo salio mal");
            return View(servicios);
        }


        //PUT: editar usuarios
        public ActionResult EditServicio(int id)
        {
            Servicios servicios = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:58727/");

                var responseTask = client.GetAsync("api/servicios/" + id.ToString());
                responseTask.Wait();

                var resultado = responseTask.Result;
                if (resultado.IsSuccessStatusCode)
                {
                    var readTask = resultado.Content.ReadAsAsync<Servicios>();
                    readTask.Wait();
                    servicios = readTask.Result;
                }
            }
            return View(servicios);
        }


        [HttpPost]
        public ActionResult EditServicio(Servicios servicios)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:58727/");

                var putTask = client.PutAsJsonAsync($"api/servicios/{servicios.id}", servicios);
                putTask.Wait();
                var resultado = putTask.Result;
                if (resultado.IsSuccessStatusCode)
                {
                    return RedirectToAction("Lista");
                }

            }
            return View(servicios);
        }


        //PUT: editar usuarios
        public ActionResult DeleteServicio(int id)
        {
            Servicios servicios = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:58727/");

                var responseTask = client.GetAsync("api/servicios/" + id.ToString());
                responseTask.Wait();

                var resultado = responseTask.Result;
                if (resultado.IsSuccessStatusCode)
                {
                    var readTask = resultado.Content.ReadAsAsync<Servicios>();
                    readTask.Wait();
                    servicios = readTask.Result;
                }
            }
            return View(servicios);
        }


        [HttpPost]
        public ActionResult DeleteServicio(Servicios servicios)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:58727/");

                var putTask = client.PutAsJsonAsync($"api/servicios/{servicios.id}", servicios);
                putTask.Wait();
                var resultado = putTask.Result;
                if (resultado.IsSuccessStatusCode)
                {
                    return RedirectToAction("Lista");
                }

            }
            return View(servicios);
        }
    }
}