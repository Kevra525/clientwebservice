﻿using ConsumidorApi.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace ConsumidorApi.Controllers
{
    public class UsuariosController : Controller
    {
        string url = "http://localhost:58727/";

        // GET: listado de usuarios
        public async Task<ActionResult> Index()
        {
            List<Usuarios> empInfo = new List<Usuarios>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(url);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //llamar a todo los usuarios
                HttpResponseMessage rsp = await client.GetAsync("api/usuarios/");
                if (rsp.IsSuccessStatusCode)
                {
                    var empResponse = rsp.Content.ReadAsStringAsync().Result;
                    empInfo = JsonConvert.DeserializeObject<List<Usuarios>>(empResponse);
                }
            }

            return View(empInfo);
        }

        //POST: crear usuario
        public ActionResult create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult create(Usuarios usuarios)
        {

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:58727/api/usuarios/");
                var postTarea = client.PostAsJsonAsync<Usuarios>("usuarios", usuarios);
                postTarea.Wait();
                var resultado = postTarea.Result;
                if (resultado.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
            }

            ModelState.AddModelError(string.Empty, "ALgo salio mal");
            return View(usuarios);
        }


        //PUT: editar usuarios
        public ActionResult Edit(int id)
        {
            Usuarios usuarios = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:58727/");

                var responseTask = client.GetAsync("api/usuarios/" + id.ToString());
                responseTask.Wait();

                var resultado = responseTask.Result;
                if (resultado.IsSuccessStatusCode)
                {
                    var readTask = resultado.Content.ReadAsAsync<Usuarios>();
                    readTask.Wait();
                    usuarios = readTask.Result;
                }
            }
            return View(usuarios);
        }


        [HttpPost]
        public ActionResult Edit(Usuarios usuarios)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:58727/");

                var putTask = client.PutAsJsonAsync($"api/usuarios/{usuarios.id}", usuarios);
                putTask.Wait();
                var resultado = putTask.Result;
                if (resultado.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }

            }
            return View(usuarios);
        }


        //PUT: editar usuarios
        public ActionResult Delete(int id)
        {
            Usuarios usuarios = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:58727/");

                var responseTask = client.GetAsync("api/usuarios/" + id.ToString());
                responseTask.Wait();

                var resultado = responseTask.Result;
                if (resultado.IsSuccessStatusCode)
                {
                    var readTask = resultado.Content.ReadAsAsync<Usuarios>();
                    readTask.Wait();
                    usuarios = readTask.Result;
                }
            }
            return View(usuarios);
        }


        [HttpPost]
        public ActionResult Delete(Usuarios usuarios)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:58727/");

                var putTask = client.PutAsJsonAsync($"api/usuarios/{usuarios.id}", usuarios);
                putTask.Wait();
                var resultado = putTask.Result;
                if (resultado.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }

            }
            return View(usuarios);
        }

    }
}